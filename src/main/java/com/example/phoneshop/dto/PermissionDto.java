package com.example.phoneshop.dto;
//Sevinch Abdisattorova 04/08/2022 11:24 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class PermissionDto {
    Integer userId;
    Integer privilegeId;
}
