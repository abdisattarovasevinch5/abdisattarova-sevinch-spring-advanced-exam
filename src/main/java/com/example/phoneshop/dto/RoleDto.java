package com.example.phoneshop.dto;
//Sevinch Abdisattorova 04/08/2022 11:34 AM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RoleDto {
    Integer userID;
    Integer roleId;
}
