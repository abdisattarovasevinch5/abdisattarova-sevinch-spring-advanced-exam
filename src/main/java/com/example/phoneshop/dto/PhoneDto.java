package com.example.phoneshop.dto;
//Sevinch Abdisattorova 04/07/2022 10:14 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
public class PhoneDto {

    @NotEmpty
    @Size(min = 2, message = "Name of the phone should contain at least two letters!")
    String name;

    @NotNull(message = "Company must be chooses!")
    Integer companyId;

    @NotNull(message = "Select at least one color!")
    List<Integer> colorIds;

    @NotNull(message = "Memory must be given!")
    Integer memoryInGb;

    @NotNull(message = "Price must be given!")
    Double price;

    @NotEmpty(message = "It is better to give some information!")
    String info;


    Integer guaranteeInMonths;

    @NotNull
    List<MultipartFile> photos;
}
