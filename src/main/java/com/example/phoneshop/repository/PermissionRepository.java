package com.example.phoneshop.repository;
//Sevinch Abdisattorova 04/08/2022 11:26 AM


import com.example.phoneshop.entity.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends JpaRepository<Privilege, Integer> {


    @Query(nativeQuery = true,value = "insert into users_privileges(user_id, privilege_id)\n" +
            "VALUES (:userId, :privilegeId)")
    void addPermissionToUser(Integer userId, Integer privilegeId);
}
