package com.example.phoneshop.repository;
//Sevinch Abdisattorova 04/08/2022 11:33 AM

import com.example.phoneshop.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {


    @Query(nativeQuery = true, value = "insert into users_roles(users_id, roles_id)\n" +
            "VALUES (:userId, :roleId)")
    void addRoleToUser(Integer userId, Integer roleId);
}
