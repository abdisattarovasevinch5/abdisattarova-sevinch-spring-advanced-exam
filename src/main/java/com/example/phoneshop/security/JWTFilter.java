package com.example.phoneshop.security;
//Sevinch Abdisattorova 04/08/2022 8:56 AM


import com.example.phoneshop.repository.UserRepository;
import com.example.phoneshop.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTFilter extends OncePerRequestFilter {

    @Autowired
    JWTProvider jwtProvider;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthService authService;
    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    protected void doFilterInternal(
            HttpServletRequest httpServletRequest,
            HttpServletResponse httpServletResponse,
            FilterChain filterChain)
            throws ServletException, IOException {
        UserDetails userDetails = getUserDetails(httpServletRequest);
        if (userDetails != null) {
            if (userDetails.isEnabled()
                    && userDetails.isAccountNonExpired()
                    && userDetails.isAccountNonLocked()
                    && userDetails.isCredentialsNonExpired()) {
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }


    /**
     * HttpServletRequest dan tokenni olib, u orqali userni olish
     *
     * @param httpServletRequest
     * @return
     */
    public UserDetails getUserDetails(HttpServletRequest httpServletRequest) {
        try {
            //REQUESTNI HEADER DAN "AUTHORIZATION" KALIT SO'Z ORQALI TOKENNI OLISH
            String tokenClient = httpServletRequest.getHeader("Authorization");
            //TOKENNI NULL GA TEKSHIRISH
            if (tokenClient != null) {
                //TOKENNI BEARER EKANLIGINI TEKSHIRISH
                if (tokenClient.startsWith("Bearer ")) {
                    //TOKENNI 7 INDEX DAN BOSHLAB KESIB OLISH
                    tokenClient = tokenClient.substring(7);
                    //TOKENNI VALID LIGINI TEKSHIRISH
                    if (jwtProvider.validateToken(tokenClient, httpServletRequest)) {
                        //USERID ni oldik tokendan
                        String usernameFromToken = jwtProvider.getUsernameFromToken(tokenClient);
                        return authService.loadUserByUsername(usernameFromToken);
                    }
                }
            }
        } catch (Exception ignored) {
        }
        return null;
    }
}
