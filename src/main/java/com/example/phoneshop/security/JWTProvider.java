package com.example.phoneshop.security;
//Sevinch Abdisattorova 04/08/2022 8:59 AM

import io.jsonwebtoken.*;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class JWTProvider {

    long expirationTime = 1000 * 36000;
    String secretKey = "secret";

    public String generateToken(String username) {
        String token = Jwts.builder()
                .setSubject(username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
        return token;
    }

    public boolean validateToken(String tokenClient, HttpServletRequest httpServletRequest) {
        try {
            Jwts
                    .parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(tokenClient);
            return true;
        } catch (ExpiredJwtException e) {
            System.err.println("Expired token");
            httpServletRequest.setAttribute("expired", e.getMessage());
        } catch (MalformedJwtException malformedJwtException) {
            System.err.println("Broken token");
        } catch (SignatureException s) {
            System.err.println("Secret key is wrong");
        } catch (UnsupportedJwtException unsupportedJwtException) {
            System.err.println("Not used token");
        } catch (IllegalArgumentException ex) {
            System.err.println("Empty token");
        }
        return false;

    }

    public String getUsernameFromToken(String tokenClient) {
        return Jwts
                .parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(tokenClient)
                .getBody()
                .getSubject();

    }
}
