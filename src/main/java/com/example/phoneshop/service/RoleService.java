package com.example.phoneshop.service;
//Sevinch Abdisattorova 04/08/2022 11:32 AM


import com.example.phoneshop.dto.RoleDto;
import com.example.phoneshop.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;


    public ResponseEntity<?> addRoleToUser(RoleDto roleDto) {
        try {
            roleRepository.addRoleToUser(roleDto.getUserID(), roleDto.getRoleId());
            return new ResponseEntity<>("Successfully added new privilege", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Could not add new privilege", HttpStatus.CONFLICT);

    }
}
