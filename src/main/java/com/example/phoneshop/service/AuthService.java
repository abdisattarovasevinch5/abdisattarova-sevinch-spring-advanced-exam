package com.example.phoneshop.service;
//Sevinch Abdisattorova 04/07/2022 9:43 PM


import com.example.phoneshop.entity.Privilege;
import com.example.phoneshop.entity.Role;
import com.example.phoneshop.entity.User;
import com.example.phoneshop.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static java.util.Collections.emptyList;

@Service
@RequiredArgsConstructor
public class AuthService implements UserDetailsService {

    private final UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> byUsername = userRepository.findByUsername(username);
        if (byUsername.isPresent()) {
            User user = byUsername.get();
            return new org.springframework.security.core.userdetails.User(user.getUsername(),
                    user.getPassword(),
                    getAuthorities(user.getRoles(), user.getPrivileges()));
        }
        throw new UsernameNotFoundException(username);
    }

    private Collection<? extends GrantedAuthority> getAuthorities(List<Role> roles, List<Privilege> privileges) {
        return getGrantedAuthorities(getPrivileges(roles, privileges));
    }

    private Collection<? extends GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }


    private List<String> getPrivileges(List<Role> roles, List<Privilege> privileges) {
        List<String> privileges1 = new ArrayList<>();
        List<Privilege> collection = new ArrayList<>();

        for (Role role : roles) {
            privileges1.add(role.getName().name());
            collection.addAll(role.getPrivileges());
        }
        collection.addAll(privileges);

        for (Privilege privilege : collection) {
            privileges1.add(privilege.getName());
        }
        return privileges1;
    }
}
