package com.example.phoneshop.service;
//Sevinch Abdisattorova 04/07/2022 10:00 PM


import com.example.phoneshop.dto.PhoneDto;
import com.example.phoneshop.entity.Attachment;
import com.example.phoneshop.entity.Color;
import com.example.phoneshop.entity.Company;
import com.example.phoneshop.entity.Phone;
import com.example.phoneshop.repository.ColorRepository;
import com.example.phoneshop.repository.CompanyRepository;
import com.example.phoneshop.repository.PhoneRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PhoneService {


    private final PhoneRepository phoneRepository;
    private final CompanyRepository companyRepository;
    private final ColorRepository colorRepository;


    public ResponseEntity<?> getAllPhones() {
        List<Phone> phones = phoneRepository.findAll();
        if (phones.size() > 0) {
            return new ResponseEntity<>(phones, HttpStatus.OK);
        }
        return new ResponseEntity<>("No phones yet!", HttpStatus.NOT_FOUND);
    }


    public ResponseEntity<?> findPhoneById(Integer id) {
        Optional<Phone> byId = phoneRepository.findById(id);
        if (byId.isPresent()) {
            return new ResponseEntity<>(byId.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>("No phone by this id", HttpStatus.NOT_FOUND);
    }


    public ResponseEntity<?> addPhone(PhoneDto phoneDto) {
        try {
            List<Attachment> attachments = new ArrayList<>();

            for (MultipartFile photo : phoneDto.getPhotos()) {

                Attachment attachment = new Attachment(photo.getContentType(),
                        photo.getSize(),
                        photo.getOriginalFilename(),
                        photo.getBytes());
                attachments.add(attachment);

            }

            Company company = companyRepository.findById(phoneDto.getCompanyId()).get();
            List<Color> colors = new ArrayList<>();
            for (Integer colorId : phoneDto.getColorIds()) {
                Color color = colorRepository.findById(colorId).get();
                colors.add(color);
            }
            Phone phone = new Phone(
                    phoneDto.getName(),
                    company,
                    colors,
                    phoneDto.getMemoryInGb(),
                    phoneDto.getPrice(),
                    phoneDto.getInfo(),
                    phoneDto.getGuaranteeInMonths(),
                    attachments
            );
            phoneRepository.save(phone);
            return new ResponseEntity<>("Successfully saved!", HttpStatus.CREATED);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Error", HttpStatus.CONFLICT);
    }


    @SneakyThrows
    public ResponseEntity<?> editPhone(PhoneDto phoneDto, Integer id) {
        Optional<Phone> byId = phoneRepository.findById(id);
        if (byId.isPresent()) {

            List<Attachment> attachments = new ArrayList<>();
            for (MultipartFile photo : phoneDto.getPhotos()) {

                Attachment attachment = new Attachment(photo.getContentType(),
                        photo.getSize(),
                        photo.getOriginalFilename(),
                        photo.getBytes());
                attachments.add(attachment);

            }

            Company company = companyRepository.findById(phoneDto.getCompanyId()).get();
            List<Color> colors = new ArrayList<>();
            for (Integer colorId : phoneDto.getColorIds()) {
                Color color = colorRepository.findById(colorId).get();
                colors.add(color);
            }
            Phone phone = byId.get();
            phone.setName(phoneDto.getName());
            phone.setCompany(company);
            phone.setColors(colors);
            phone.setPhotos(attachments);
            phone.setInfo(phoneDto.getInfo());
            phone.setGuaranteeInMonths(phoneDto.getGuaranteeInMonths());
            phone.setMemoryInGb(phoneDto.getMemoryInGb());
            phone.setPrice(phoneDto.getPrice());

            phoneRepository.save(phone);
            return new ResponseEntity<>("Successfully updated!", HttpStatus.OK);
        }
        return new ResponseEntity<>("Error", HttpStatus.NOT_FOUND);
    }


    public ResponseEntity<?> deletePhoneById(Integer id) {
        try {
            phoneRepository.deleteById(id);
            return new ResponseEntity<>("Successfully deleted!", HttpStatus.NO_CONTENT);
        } catch (Exception ignored) {
        }
        return new ResponseEntity<>("Not deleted", HttpStatus.CONFLICT);
    }
}
