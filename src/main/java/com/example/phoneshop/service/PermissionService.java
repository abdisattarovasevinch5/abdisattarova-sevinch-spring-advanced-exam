package com.example.phoneshop.service;
//Sevinch Abdisattorova 04/08/2022 11:25 AM


import com.example.phoneshop.dto.PermissionDto;
import com.example.phoneshop.repository.PermissionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PermissionService {

    private final PermissionRepository permissionRepository;

    public ResponseEntity<?> addPermissionToUser(PermissionDto permissionDto) {
        try {
            permissionRepository.addPermissionToUser(permissionDto.getUserId(), permissionDto.getPrivilegeId());
            return new ResponseEntity<>("Successfully added new privilege", HttpStatus.CREATED);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Could not add new privilege", HttpStatus.CONFLICT);
    }
}
