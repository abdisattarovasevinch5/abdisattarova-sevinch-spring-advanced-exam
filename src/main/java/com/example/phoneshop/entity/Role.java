package com.example.phoneshop.entity;
//Sevinch Abdisattorova 04/07/2022 2:41 PM


import com.example.phoneshop.entity.enums.RoleEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Collection;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "roles")
public class Role extends AbsEntity {

    @Enumerated(EnumType.STRING)
    RoleEnum name;

    @Cascade(value = org.hibernate.annotations.CascadeType.ALL)
    @ManyToMany
    @JoinTable(
            name = "roles_privileges",
            joinColumns = {@JoinColumn(name = "role_id")},
            inverseJoinColumns = {@JoinColumn(name = "privilege_id")})
    Collection<Privilege> privileges;
}
