package com.example.phoneshop.entity;
//Sevinch Abdisattorova 04/07/2022 10:18 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "colors")
public class Color extends AbsEntity {

    String name;
}
