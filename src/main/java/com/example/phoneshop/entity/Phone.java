package com.example.phoneshop.entity;
//Sevinch Abdisattorova 04/07/2022 2:24 PM

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@PackagePrivate
@Entity(name = "phones")
public class Phone extends AbsEntity {

    String name;

    @ManyToOne
    Company company;

    @ManyToMany
    List<Color> colors;

    Integer memoryInGb;

    Double price;

    String info;

    Integer guaranteeInMonths;

    @OneToMany(cascade = CascadeType.ALL)
    List<Attachment> photos;


}
