package com.example.phoneshop.entity;
//Sevinch Abdisattorova 04/08/2022 8:40 AM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "privileges")
@PackagePrivate
public class Privilege extends AbsEntity {
    String name;
}
