package com.example.phoneshop.entity;
//Sevinch Abdisattorova 03/14/2022 6:45 PM


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.PackagePrivate;

import javax.persistence.Column;
import javax.persistence.Entity;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity(name = "attachments")
@PackagePrivate
public class Attachment extends AbsEntity {

    @Column(name = "content_type")
    String contentType;

    Long size;

    String name;

    byte[] data;

}
