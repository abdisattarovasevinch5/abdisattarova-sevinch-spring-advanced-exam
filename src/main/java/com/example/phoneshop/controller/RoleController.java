package com.example.phoneshop.controller;
//Sevinch Abdisattorova 04/08/2022 11:32 AM


import com.example.phoneshop.dto.RoleDto;
import com.example.phoneshop.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/role")
@RequiredArgsConstructor
public class RoleController {


    private final RoleService roleService;


    @PostMapping
    public ResponseEntity<?> addRoletoUser(RoleDto roleDto){
        return roleService.addRoleToUser(roleDto);
    }
}
