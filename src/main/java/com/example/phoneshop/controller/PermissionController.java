package com.example.phoneshop.controller;
//Sevinch Abdisattorova 04/08/2022 11:22 AM


import com.example.phoneshop.dto.PermissionDto;
import com.example.phoneshop.service.PermissionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/permission")
@RequiredArgsConstructor
public class PermissionController {


    private final PermissionService permissionService;

    @PostMapping
    public ResponseEntity<?> addRoleToUser(PermissionDto permissionDto) {
        return permissionService.addPermissionToUser(permissionDto);
    }

}
