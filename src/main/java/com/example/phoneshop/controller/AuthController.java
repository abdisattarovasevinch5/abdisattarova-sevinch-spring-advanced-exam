package com.example.phoneshop.controller;
//Sevinch Abdisattorova 04/07/2022 9:41 PM

import com.example.phoneshop.dto.UserDto;
import com.example.phoneshop.security.JWTProvider;
import com.example.phoneshop.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {


    private final AuthenticationManager authenticationManager;
    private final AuthService authService;
    private final JWTProvider jwtProvider;


    @PreAuthorize(value = "permitAll()")
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody UserDto userDto) {

        UserDetails userDetails = authService.loadUserByUsername(userDto.getUsername());
        String generatedToken = jwtProvider.generateToken(userDetails.getUsername());
        return ResponseEntity.ok(generatedToken);
    }


}
