package com.example.phoneshop.controller;
//Sevinch Abdisattorova 04/07/2022 9:41 PM

import com.example.phoneshop.dto.PhoneDto;
import com.example.phoneshop.service.PhoneService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/phone")
public class PhoneController {

    private final PhoneService phoneService;

//    @PreAuthorize("permitAll()")
    @GetMapping
    public ResponseEntity<?> getAllPhones() {
        return phoneService.getAllPhones();
    }


//    @PreAuthorize(value = "hasAnyAuthority('ADMIN','READ_PRODUCT')")
    @GetMapping("/{id}")
    public ResponseEntity<?> getPhoneById(@PathVariable Integer id) {
        return phoneService.findPhoneById(id);
    }


//    @PreAuthorize(value = "hasAuthority('CREATE_PRODUCT')")
    @PostMapping(consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> addPhone(
            @Valid PhoneDto phoneDto,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            // TODO: 04/07/2022
//            return "";
        }
        return phoneService.addPhone(phoneDto);
    }


//    @PreAuthorize(value = "hasAuthority('UPDATE_PRODUCT')")
    @PutMapping(path = "/{id}", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<?> editPhone(
            @Valid PhoneDto phoneDto,
            BindingResult bindingResult,
            @PathVariable Integer id) {

        if (bindingResult.hasErrors()) {
            // TODO: 04/07/2022
//            return "";
        }
        return phoneService.editPhone(phoneDto, id);
    }

//    @PreAuthorize(value = "hasAuthority('DELETE_PRODUCT')")
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePhone(@PathVariable Integer id) {
        return phoneService.deletePhoneById(id);
    }
}
