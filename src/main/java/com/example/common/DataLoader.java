package com.example.common;
//Sevinch Abdisattorova 04/08/2022 11:40 AM

import com.example.phoneshop.entity.Privilege;
import com.example.phoneshop.entity.Role;
import com.example.phoneshop.entity.User;
import com.example.phoneshop.entity.enums.RoleEnum;
import com.example.phoneshop.repository.PermissionRepository;
import com.example.phoneshop.repository.PhoneRepository;
import com.example.phoneshop.repository.RoleRepository;
import com.example.phoneshop.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    String initMode;

    @Autowired
    UserRepository userRepository;
    @Autowired
    PhoneRepository phoneRepository;
    @Autowired
    PermissionRepository permissionRepository;
    @Autowired
    RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {
            Privilege readAllProducts = new Privilege("READ_ALL_PRODUCTS");
            Privilege readProduct = new Privilege("READ_PRODUCT");
            Privilege createProduct = new Privilege("CREATE_PRODUCT");
            Privilege deleteProduct = new Privilege("DELETE_PRODUCT");
            Privilege updateProduct = new Privilege(" UPDATE_PRODUCT");
            List<Privilege> privileges = new ArrayList<>(Arrays.asList(readAllProducts,
                    readProduct,
                    createProduct,
                    updateProduct,
                    deleteProduct));
            permissionRepository.saveAll(privileges);

            Role admin = new Role(RoleEnum.ADMIN, new ArrayList<>(Arrays.asList(createProduct)));
            Role user = new Role(RoleEnum.USER, new ArrayList<>());


            User user1 = new User("User",
                    "user",
                    "1",
                    new ArrayList<>(Arrays.asList(user)),
                    null);

            User admin1 = new User("Admin Main",
                    "admin1",
                    "1",
                    new ArrayList<>(Arrays.asList(admin)),
                    new ArrayList<>(Arrays.asList(readAllProducts,
                            readProduct,
                            createProduct,
                            deleteProduct,
                            updateProduct)));

            User admin2 = new User("Admin Main",
                    "admin1",
                    "1",
                    new ArrayList<>(Arrays.asList(admin)), null);

            userRepository.save(user1);
            userRepository.save(admin1);
            userRepository.save(admin2);
        }
    }
}
